#!/bin/sh

set -e
# This entrypoint is used to play nicely with the current cookiecutter configuration.
# Since docker-compose relies heavily on environment variables itself for configuration, we'd have to define multiple
# environment variables just to support cookiecutter out of the box. That makes no sense, so this little entrypoint
# does all this for us.
export REDIS_URL=redis://redis:6379

# usage: file_env VAR [DEFAULT]
#    ie: file_env 'XYZ_DB_PASSWORD' 'example'
# (will allow for "$XYZ_DB_PASSWORD_FILE" to fill in the value of
#  "$XYZ_DB_PASSWORD" from a file, especially for Docker's secrets feature)
file_env() {
    local var="$1"
    local fileVar="${var}_FILE"
    local def="${2:-}"
    if [ "${!var:-}" ] && [ "${!fileVar:-}" ]; then
        echo >&2 "error: both $var and $fileVar are set (but are exclusive)"
        exit 1
    fi
    local val="$def"
    if [ "${!var:-}" ]; then
        val="${!var}"
    elif [ "${!fileVar:-}" ]; then
        val="$(< "${!fileVar}")"
    fi
    export "$var"="$val"
    unset "$fileVar"
}

file_env 'POSTGRES_HOST'
file_env 'POSTGRES_PORT'
file_env 'POSTGRES_USER'
file_env 'POSTGRES_PASS'

#POSTGRES_HOST=${POSTGRES_HOST:-}
#POSTGRES_PORT=${POSTGRES_PORT:-}
#POSTGRES_USER=${POSTGRES_USER:-}
#POSTGRES_PASS=${POSTGRES_PASS:-}
PGBOUNCER_AUTH_TYPE=${PGBOUNCER_AUTH_TYPE:-}
PGBOUNCER_MAX_CLIENT_CONN=${PGBOUNCER_MAX_CLIENT_CONN:-}
PGBOUNCER_DEFAULT_POOL_SIZE=${PGBOUNCER_DEFAULT_POOL_SIZE:-}
PGBOUNCER_SERVER_IDLE_TIMEOUT=${PGBOUNCER_SERVER_IDLE_TIMEOUT:-}

if [ -f /etc/pgbouncer/pgbconf.ini ]
then
    echo "file: /etc/pgbouncer/pgbconf.ini already exists... continue"
else
    echo "Creating a new file from env vars: /etc/pgbouncer/pgbconf.ini"
    cat << EOF > /etc/pgbouncer/pgbconf.ini
[databases]
* = host=${POSTGRES_HOST} port=${POSTGRES_PORT}

[pgbouncer]
logfile = /var/log/pgbouncer/pgbouncer.log
pidfile = /var/run/pgbouncer/pgbouncer.pid
listen_addr = 0.0.0.0
listen_port = 6432
unix_socket_dir = /var/run/pgbouncer
auth_type = ${PGBOUNCER_AUTH_TYPE}
auth_file = /etc/pgbouncer/userlist.txt
pool_mode = session
server_reset_query = DISCARD ALL
max_client_conn = ${PGBOUNCER_MAX_CLIENT_CONN}
default_pool_size = ${PGBOUNCER_DEFAULT_POOL_SIZE}
ignore_startup_parameters = extra_float_digits
server_idle_timeout = ${PGBOUNCER_SERVER_IDLE_TIMEOUT}
EOF
fi

if [-s /etc/pgbouncer/userlist.txt ]
then
    echo "file: /etc/pgbouncer/userlist.txt already exists... continue"
else
    echo '"'"${POSTGRES_USER}"'" "'"${POSTGRES_PASS}"'"'  > /etc/pgbouncer/userlist.txt
fi

chown -R pgbouncer:pgbouncer /etc/pgbouncer
chmod 640 /etc/pgbouncer/userlist.txt

pgbouncer -u pgbouncer /etc/pgbouncer/pgbconf.ini
